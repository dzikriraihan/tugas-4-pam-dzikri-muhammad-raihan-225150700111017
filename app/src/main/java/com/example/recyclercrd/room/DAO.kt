package com.example.recyclercrd.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface DAO {
    @Insert
    fun addMenu(menu: Menu)

    @Update
    fun updateMenu(menu: Menu)

    @Delete
    fun delMenu(menu: Menu)

    @Query("SELECT * FROM Menu")
    fun getMenu(): List<Menu>
}